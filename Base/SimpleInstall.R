# 打开帮助文档
help.start()
# 安装三方库
install.packages('vcd')
# 打开三方库的帮助文档
help(package='vcd')
# 加载三方库
library(vcd)
# 获取数据集的描述
help(Arthritis)
# 显示数据集
Arthritis
# 运行自带样例
example(Arthritis)
# 退出
q()


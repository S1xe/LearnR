summary(mtcars$mpg)
plot(mtcars$mpg,mtcars$disp)
plot(mtcars$mpg,mtcars$wt)

attach(mtcars)
	summary(mpg)
	plot(mpg,disp)
	plot(mpg,wt)
detach(mtcars)

witch(mtcars,{
	print(summary(mpg))
	plot(mpg,disp)
	plot(mpg,wt)
})
